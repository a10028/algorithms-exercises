import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestStringChecker {

    @Test
    public void test1() {
        String str = "{{sded{{fekgjre}klj}lkreg}345}";
        boolean isRightString = StringChecker.parse(str);
        assertTrue(isRightString);
    }

    @Test
    public void test2() {
        String str = "{{sded{{fekgjre}klj}lkr5}";
        boolean isRightString = StringChecker.parse(str);
        assertFalse(isRightString);
    }

    @Test
    public void test3() {
        String str = "}klj}lkr5}";
        boolean isRightString = StringChecker.parse(str);
        assertFalse(isRightString);
    }

    @Test
    public void test4() {
        String str = "{{sded{";
        boolean isRightString = StringChecker.parse(str);
        assertFalse(isRightString);
    }

    @Test
    public void test5() {
        String str = "}}{{";
        boolean isRightString = StringChecker.parse(str);
        assertFalse(isRightString);
    }

    @Test
    public void test6() {
        String str = "{{";
        boolean isRightString = StringChecker.parse(str);
        assertFalse(isRightString);
    }

    @Test
    public void testMapReduce1() {
        String str1 = "{{d{{fj{3345}re}klj}lk}r5}";
        String str2 = "{{sded{klj}lkr5}a}";
        String str3 = "{sded{{fekg}jre}klj}";

        boolean isRightString = StringChecker.parse(str1, str2, str3);
        assertTrue(isRightString);
    }

    @Test
    public void testMapReduce2() {
        String str1 = "{d{{fj{33}re}klj}lk}r5}";
        String str2 = "{{sded{klj}lkr5}a}";
        String str3 = "{sded{{fekg}jre}klj";

        boolean isRightString = StringChecker.parse(str1, str2, str3);
        assertFalse(isRightString);
    }
}
