import org.junit.jupiter.api.Test;
import util.Heap;
import util.Node;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCheckBst {

    @Test
    public void test1() {
        Node validTree = Heap.of(new long[] {
                     90,
                50,       100,
             25,   60,   95,   110,
        });

        boolean isValid = CheckBst.check(validTree);
        assertTrue(isValid);
    }

    @Test
    public void test2() {
        Node invalidTree = Heap.of(new long[] {
                     90,
                50,       100,
               125,   60,   95,   110,
        });

        boolean isValid = CheckBst.check(invalidTree);
        assertFalse(isValid);
    }

    @Test
    public void test3() {
        Node invalidTree = Heap.of(new long[] {
                         90,
                     50,       100,
                25,   60,   95,   10,
        });

        boolean isValid = CheckBst.check(invalidTree);
        assertFalse(isValid);
    }
}
