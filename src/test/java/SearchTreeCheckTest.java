import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import old.search_tree_check.BinarySearchTreeCheck;
import old.search_tree_check.Node;

public class SearchTreeCheckTest {

    public static Node createInvalidBinaryTree() {

/*
             90
        50       70
     25   60   69   81
*/

        Node leftLeft = Node.builder()
                .value(25).build();
        Node leftRight = Node.builder()
                .value(60).build();

        Node left = Node.builder()
                .leftChild(leftLeft)
                .rightChild(leftRight)
                .value(50)
                .build();

        Node rightLeft = Node.builder()
                .value(69).build();
        Node rightRight = Node.builder()
                .value(81).build();

        Node right = Node.builder()
                .leftChild(rightLeft)
                .rightChild(rightRight)
                .value(70)
                .build();

        Node root = Node.builder()
                .leftChild(left)
                .rightChild(right)
                .value(90)
                .build();

        return root;
    }

    @Test
    public void testInvalid() {

        boolean isValid = BinarySearchTreeCheck.isValid(createInvalidBinaryTree());

        Assertions.assertFalse(isValid);
    }

    public static Node createValidBinaryTree() {

/*
             90
        50       100
     25   60   95   110
*/

        Node leftLeft = Node.builder()
                .value(25).build();
        Node leftRight = Node.builder()
                .value(60).build();

        Node left = Node.builder()
                .leftChild(leftLeft)
                .rightChild(leftRight)
                .value(50)
                .build();

        Node rightLeft = Node.builder()
                .value(95).build();
        Node rightRight = Node.builder()
                .value(110).build();

        Node right = Node.builder()
                .leftChild(rightLeft)
                .rightChild(rightRight)
                .value(100)
                .build();

        Node root = Node.builder()
                .leftChild(left)
                .rightChild(right)
                .value(90)
                .build();

        return root;
    }

    @Test
    public void testValid() {

        boolean isOk = BinarySearchTreeCheck.isValid(createValidBinaryTree());

        Assertions.assertTrue(isOk);
    }


}
