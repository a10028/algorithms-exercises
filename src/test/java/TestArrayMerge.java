import org.junit.jupiter.api.Test;

import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestArrayMerge {

    @Test
    public void test1() {
        LongStream stream1 = LongStream.of(12, 56, 89, 94);
        LongStream stream2 = LongStream.of(12, 66, 75);
        LongStream stream3 = LongStream.of(0, 6, 175);
        long[] expected = {0, 6, 12, 12, 56, 66, 75, 89, 94, 175};
        long[] result = ArraysMerge.merge(stream3.toArray(), stream1.toArray(), stream2.toArray());
        assertArrayEquals(expected, result);
    }

    @Test
    public void test2() {
        LongStream stream1 = LongStream.of(94);
        LongStream stream2 = LongStream.of(12);
        LongStream stream3 = LongStream.of(75);
        long[] expected = {12, 75, 94};
        long[] result = ArraysMerge.merge(stream3.toArray(), stream1.toArray(), stream2.toArray());
        assertArrayEquals(expected, result);
    }
}

