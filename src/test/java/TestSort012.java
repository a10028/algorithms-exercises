import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestSort012 {

    @Test
    public void test() {
        long[] arr1  = {0,0,0,0,1,1,1,2,2};
        long[] sorted1 = {0,0,0,0,1,1,1,2,2};

        long[] arr2  = {0,0,0,1,1,1,2,2};
        long[] sorted2 = {0,0,0,1,1,1,2,2};

        Sort012.sort(arr1);
        assertArrayEquals(sorted1, arr1);
        System.out.println("Arr2 = " + arr2);
        Sort012.sort(arr2);
        assertArrayEquals(sorted2, arr2);
        System.out.println("Arr2 = " + arr2);
    }

    @Test
    public void test1() {
        long[] arr1   = {1,2,0,1,2,1,1,0,2};
        long[] sorted = {0,0,1,1,1,1,2,2,2};

        Sort012.sort(arr1);
        assertArrayEquals(sorted, arr1);
        System.out.println("Arr1 = " + arr1);
    }

    @Test
    public void test2() {
        long[] arr1   = {1,2,2,0,1,0,2,1,2,0,2,1};
        long[] sorted = {0,0,0,1,1,1,1,2,2,2,2,2};

        Sort012.sort(arr1);
        assertArrayEquals(sorted, arr1);
        System.out.println("Arr1 = " + arr1);
    }

    @Test
    public void test3() {
        long[] arr1   = {2,2,2,0,0,0,1,1};
        long[] sorted = {0,0,0,1,1,2,2,2};

        Sort012.sort(arr1);
        assertArrayEquals(sorted, arr1);
        System.out.println("Arr1 = " + arr1);
    }

    @Test
    public void test4() {
        long[] arr1   = {1,1,2,2,2,0,0,0};
        long[] sorted = {0,0,0,1,1,2,2,2};

        Sort012.sort(arr1);
        assertArrayEquals(sorted, arr1);
        System.out.println("Arr1 = " + arr1);
    }

}
