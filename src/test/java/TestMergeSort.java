import old.mergesort.MergeSort;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMergeSort {

    @Test
    public void t() {

        Object[] objArr = new Long[10];

        Long[] longArr = (Long[]) objArr;

    }

    @Test
    public void testSorting() {
        Random localRandom = new Random();

        List<Long> unsorted = localRandom
                .longs(0, 1001)
                .limit(11)
                .boxed()
                .collect(toList());

        System.out.println("Collection values: " + unsorted);

        MergeSort mergeSort = new MergeSort();

        Object[] sortedObj =  mergeSort.sort(unsorted.toArray());
        //Object[] sorted = (Long[]) sortedObj;

        System.out.println("Collection sorted: " + List.of(sortedObj));
        assertEquals(unsorted.size(), sortedObj.length);
    }
}
