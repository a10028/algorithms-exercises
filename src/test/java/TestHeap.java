import org.junit.jupiter.api.Test;
import util.Heap;
import util.Node;

public class TestHeap {

    @Test
    public void test1() {
        long[] arr = new long[] {
                               5,
                     3,                4,
                    7, 8,             6, 9,
                12, 13, 14, 15,   16, 17, 18, 19
        };
        Node tree = Heap.of(arr);
        System.out.println(tree);
    }

}
