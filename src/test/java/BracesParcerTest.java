import old.braces_parsing.BracesChecker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BracesParcerTest {

    @Test
    public void testSuccess () {

        String str = "(((((1sds(232)232)))(12(1223)23)))";

        boolean isOk = BracesChecker.check(str.toCharArray());

        Assertions.assertEquals(true, isOk);
    }

    @Test
    public void testSuccess1 () {

        String str = "(dvfdg)";

        boolean isOk = BracesChecker.check(str.toCharArray());

        Assertions.assertEquals(true, isOk);
    }

    @Test
    public void testFail () {

        String str = "(((1sds(232)232)))(12(1223)23))9()";

        boolean isOk = BracesChecker.check(str.toCharArray());

        Assertions.assertFalse(isOk);
    }

    @Test
    public void testFail2 () {

        String str = "))";

        boolean isOk = BracesChecker.check(str.toCharArray());

        Assertions.assertFalse(isOk);
    }

}
