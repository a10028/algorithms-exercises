import org.junit.jupiter.api.Test;
import util.AssertTreeEquals;
import util.Heap;
import util.Node;

public class ArrayToBstTest {

    @Test
    public void test1() {
        Node expected = Heap.of(new Integer[]{
                              5,
                    2,                   9,
                1,      4,         8,          10,
              0,null,  3,null,  7,null,    null,null
        });
        long[] arr = new long[]{0, 1, 2, 3, 4, 5, 7, 8, 9, 10};
        Node tree = ArrayToBinarySearchTree.createTree(arr);
        System.out.println("BST = " + tree);
        AssertTreeEquals.assertEquals(expected, tree);
    }

    @Test
    public void test2() {
        Node expected = Heap.of(new long[]{
                               7,
                       3,               11,
                  1,       5,      9,        13,
                0, 2,    4, 6,    8, 10,    12, 14
        });
        long[] arr = new long[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        Node tree = ArrayToBinarySearchTree.createTree(arr);
        AssertTreeEquals.assertEquals(expected, tree);
        System.out.println("BST = " + tree);
    }
}
