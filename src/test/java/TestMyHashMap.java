import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestMyHashMap {

    @Test
    public void test1() {

        MyHashMap<String, Integer> myMap = new MyHashMap<>();

        myMap.add("first", 1);
        myMap.add("second", 2);
        myMap.add("second", 22);
        myMap.add("third", 3);
        myMap.add("third", 3);

        assertEquals(3, myMap.size());
    }

    @Test
    public void test2() {

        MyHashMap<String, Integer> myMap = new MyHashMap<>();

        myMap.add("first", 1);
        myMap.add("second", 2);
        myMap.add("second", 22);
        myMap.add("third", 3);
        myMap.add("third", 3);
        assertEquals(3, myMap.size());

        Integer secondVal = myMap.find("second");
        assertEquals(22, secondVal);

        Integer firstVal = myMap.find("Fecond");
        assertNull(firstVal);
    }

    @Test
    public void test3() {

        MyHashMap<String, Integer> myMap = new MyHashMap<>();
        assertEquals(0, myMap.size());

        myMap.add("first", 1);
        myMap.add("second", 2);
        assertEquals(2, myMap.size());

        Integer secondVal = myMap.remove("second");
        assertEquals(2, secondVal);
        assertEquals(1, myMap.size());

        myMap.add("third", 3);
        assertEquals(2, myMap.size());

        myMap.add("third", 3);
        assertEquals(2, myMap.size());

        Integer firstVal = myMap.remove("first");
        assertEquals(1, firstVal);
        assertEquals(1, myMap.size());
    }
}
