import org.junit.jupiter.api.Test;
import util.AssertTreeEquals;
import util.Heap;
import util.Node;

public class TestRevertBinaryTree {

    @Test
    public void test1() {
        Node tree = Heap.of(new Integer[]{
                     1,
                2,        3,
              4, 5,    null, 7
        });
        Node expected = Heap.of(new Integer[]{
                       1,
                  3,        2,
               7, null,    5, 4
        });

        Node reverted = RevertBinaryTree.revert(tree);
        AssertTreeEquals.assertEquals(expected, reverted);
    }

    @Test
    public void test2() {
        Node tree = Heap.of(new Integer[]{
                1,
                3,        2,
                7, null,    5, 4
        });
        Node expected = Heap.of(new Integer[]{
                      1,
                2,        3,
              4, 5,    null, 7
        });

        Node reverted = RevertBinaryTree.revert(tree);
        AssertTreeEquals.assertEquals(expected, reverted);
    }
}
