import org.junit.jupiter.api.Test;
import util.Heap;
import util.Node;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestBstFindValue {

    @Test
    public void test() {
        Node bst = Heap.of(new long[]{
                    14,
            7,             18,
          4,   9,       16,    20,
         2,5, 8,10,   13,17, 19,22
        });

        Node node9 = BstFirstHigherKeyFinder.findFirstKeyHigher(bst, 8);
        assertEquals(9, node9.getKey());
    }

    @Test
    public void test2() {
        Node bst = Heap.of(new long[]{
                          14,
                   7,             18,
                4,   9,       16,    20,
               2,5, 8,10,   13,17,   19,22
        });

        Node node = BstFirstHigherKeyFinder.findFirstKeyHigher(bst, 9);
        assertEquals(10, node.getKey());
    }

    @Test
    public void test3() {
        Node bst = Heap.of(new long[]{
                       14,
                  7,             18,
                4,   9,       16,    20,
               2,5, 8,10,   13,17, 19,22
        });

        Node node = BstFirstHigherKeyFinder.findFirstKeyHigher(bst, 13);
        assertEquals(14, node.getKey());
    }

    @Test
    public void test4() {
        Node bst = Heap.of(new long[]{
                           14,
                   7,             18,
                4,   9,       16,    20,
                2,5, 8,10,   13,17, 19,22
        });

        Node node = BstFirstHigherKeyFinder.findFirstKeyHigher(bst, 22);
        assertNull(node);
    }

    @Test
    public void test5() {
        Node bst = Heap.of(new long[]{
                         14,
                   7,             18,
                4,   9,       16,    20,
                2,5, 8,10,   13,17, 19,22
        });

        Node node = BstFirstHigherKeyFinder.findFirstKeyHigher(bst, 19);
        assertEquals(20, node.getKey());
    }


}
