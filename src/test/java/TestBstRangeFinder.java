import org.junit.jupiter.api.Test;
import util.Heap;
import util.Node;

import java.util.Arrays;
import java.util.List;

public class TestBstRangeFinder {

    @Test
    public void test() {
        Node bst = Heap.of(new long[]{
                         14,
                   7,             18,
                4,   9,       16,    20,
                2,5, 8,10,   13,17,   19,22
        });

        List<Long> range = BstRangeFinder.findRange(bst, 9, 16);
        System.out.println("Range = " + Arrays.toString(range.toArray()));
    }

    @Test
    public void test2() {
        Node bst = Heap.of(new long[]{
                            14,
                     7,             18,
                  4,   9,       16,    20,
                2,5, 8,10,   13,17,   19,22
        });

        List<Long> range = BstRangeFinder.findRange(bst, 17, 23);
        System.out.println("Range = " + Arrays.toString(range.toArray()));
    }

    @Test
    public void test3() {
        Node bst = Heap.of(new long[]{
                          14,
                   7,             18,
                4,   9,       16,    20,
               2,5, 8,10,   13,17,   19,22
        });

        List<Long> range = BstRangeFinder.findRange(bst, 23, 230);
        System.out.println("Range = " + Arrays.toString(range.toArray()));
    }
}
