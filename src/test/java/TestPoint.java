import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class TestPoint {

    @Test
    public void test() {
        int count = 2;

        int [] target = new int[] {0,0};
        int [][] expected = new int[][] {{3,3}, {-2,4}};

        int[][] coords = new int[][]{
                {3,3},{5,-1},{-2,4}
        };

        int[][] result = KClosestPoints.getKClosestPoints(coords, count);
        assertArrayEquals(expected, result);
        System.out.println("res = " + Arrays.toString(result));
    }

    @Test
    public void test2() {
        int count = 2;

        int [] target = new int[] {0,0};
        int [][] expected = new int[][] {{-2,4}, {3,3}};

        int[][] coords = new int[][]{
                {3,3},{5,-1},{-2,4}
        };

        int[][] result = KClosestPoints.getKClosestPointsPq(coords, count);
        assertArrayEquals(expected, result);
        System.out.println("res = " + Arrays.toString(result));
    }
}
