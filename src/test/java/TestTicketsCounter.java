import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import old.tikets_count.TicketsCounter;

public class TestTicketsCounter {

    @Test
    public void testSlow1() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {2, 3, 2, 4};
        int result = counter.countSlow(queue, 2);
        Assertions.assertEquals(7, result);

    }

    @Test
    public void testSlow2() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {1, 1, 1, 10};
        int result = counter.countSlow(queue, 3);
        Assertions.assertEquals(13, result);
    }

    @Test
    public void testSlow3() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {10, 5, 3, 2};
        int result = counter.countSlow(queue, 0);
        Assertions.assertEquals(20, result);
    }

    @Test
    public void testFast0() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {2, 3, 2, 5, 1};
/*
        [2]++
        [3]++
        [2]++ =
        [5]+
        [1]+
*/
        int result = counter.countFast(queue, 2);
        Assertions.assertEquals(8, result);
    }

    @Test
    public void testFast1() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {2, 3, 2, 4};
        int result = counter.countSlow(queue, 2);
        Assertions.assertEquals(7, result);

    }

    @Test
    public void testFast2() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {1, 1, 1, 10};
        int result = counter.countSlow(queue, 3);
        Assertions.assertEquals(13, result);
    }

    @Test
    public void testFast3() {
        TicketsCounter counter = new TicketsCounter();

        Integer[] queue = new Integer[] {10, 5, 3, 2};
        int result = counter.countSlow(queue, 0);
        Assertions.assertEquals(20, result);
    }
}
