import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestFindNoPaired {

    @Test
    public void test1() {
        long[] arr1 = new long[] {0,0,1,1,2,2,3,3,4,4,5,5,6,/*6,*/7,7,8,8,9,9,10,10,11,11,12,12 };
        long val = FindNoPairedValue.find(arr1);
        assertThat(val).isEqualTo(6);
    }

    @Test
    public void test2() {
        long[] arr1 = new long[] {0,0,1,/*1,*/2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12 };
        long val = FindNoPairedValue.find(arr1);
        assertThat(val).isEqualTo(1);
    }

    @Test
    public void test3() {
        long[] arr1 = new long[] {0,/*0,*/1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12 };
        long val = FindNoPairedValue.find(arr1);
        assertThat(val).isEqualTo(0);
    }

    @Test
    public void test4() {
        long[] arr1 = new long[] {0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,/*10,*/10,11,11,12,12 };
        long val = FindNoPairedValue.find(arr1);
        assertThat(val).isEqualTo(10);
    }

    @Test
    public void test5() {
        long[] arr1 = new long[] {0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12/*,12*/ };
        long val = FindNoPairedValue.find(arr1);
        assertThat(val).isEqualTo(12);
    }
}
