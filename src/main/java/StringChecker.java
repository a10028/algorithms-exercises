import org.apache.commons.lang3.StringUtils;

import java.util.ArrayDeque;
import java.util.Arrays;

/*
Дана произвольная строка со скобками.
Написать программу, которая ответит на вопрос правильно ли расставлены скобки
(количество открывающихся равно количеству закрывающих,
и для каждой открывающейся есть соответствующая закрывающаяся)? Надо решить за N.
*/

// time - o(n)
// space - o(n) - память для стека
public class StringChecker {

    private static final char BR_LEFT = '{';
    private static final char BR_RIGHT = '}';
    private ArrayDeque<Character> stack = new ArrayDeque<>();

    private StringChecker() {
    }

    public static boolean parse(String... arrOfStr) {
        return Arrays.stream(arrOfStr)
                .map(StringChecker::parse)
                .reduce(true, (res1, res2) -> res1 && res2);
    }

    public static boolean parse(String str) {
        return new StringChecker().parseInternal(str);
    }

    private boolean parseInternal(String str) {
        if (StringUtils.isBlank(str)) {
            return Boolean.FALSE;
        }

        for (int idx = 0; idx < str.length(); ++idx) {
            char currentChar = str.charAt(idx);
            Character topChar = stack.peek();
            boolean isOk = true;
            if (currentChar == BR_LEFT) {
                isOk = handleLeft(topChar);
            } else if (currentChar == BR_RIGHT) {
                isOk = handleRight(topChar);
            }

            if (!isOk) {
                return false;
            }
        }

        return stack.size() == 0;
    }

    private boolean handleLeft(Character topChar) {
        if (topChar == null || topChar == BR_LEFT) {
            stack.push(BR_LEFT);
            return true;
        }

        return false;
    }

    private boolean handleRight(Character topChar) {
        if (topChar == null || topChar == BR_RIGHT) {
            return false;
        }

        Character removedTopChar = stack.pop();
        return true;
    }


}
