package util;

import lombok.*;

@Data(staticConstructor = "of")
public class Node {
    private final long key;
    private final Node left;
    private final Node right;
}
