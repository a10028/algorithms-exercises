package util;

public class AssertTreeEquals {

    public static void assertEquals(Node expected, Node result) {
        if(expected == null && result == null) {
            return;
        }
        if(expected != null && result == null) {
            throw new AssertionError(String.format("Expected = %d, found = 'null'", expected.getKey()));
        }
        if(result != null && expected == null) {
            throw new AssertionError(String.format("Expected = 'null', found = %d", result.getKey()));
        }
        if(expected.getKey() != result.getKey()) {
            throw new AssertionError(String.format("Expected = %d, found = %d", expected.getKey(), result.getKey()));
        }

        assertEquals(expected.getLeft(), result.getLeft());
        assertEquals(expected.getRight(), result.getRight());
    }

}
