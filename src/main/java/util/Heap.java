package util;

import java.util.Objects;

public class Heap {

    public static Node of(long[] arr) {
        Objects.requireNonNull(arr);

        int idx = 0;
        Node root = internalOf(arr, idx);
        Objects.requireNonNull(root);

        return root;
    }

    private static Node internalOf(long[] arr, int idx) {
        if (idx >= arr.length) {
            return null;
        }

        long key = arr[idx];

        int childLeftIdx = 2 * idx + 1;
        Node childLeft = internalOf(arr, childLeftIdx);

        int childRightIdx = 2 * idx + 2;
        Node childRight = internalOf(arr, childRightIdx);

        return Node.of(key, childLeft, childRight);
    }

    public static Node of(Integer[] arr) {
        Objects.requireNonNull(arr);

        int idx = 0;
        Node root = internalOf(arr, idx);
        Objects.requireNonNull(root);

        return root;
    }

    private static Node internalOf(Integer[] arr, int idx) {
        if (idx >= arr.length) {
            return null;
        }

        Integer key = arr[idx];
        if (key == null) {
            return null;
        }

        int childLeftIdx = 2 * idx + 1;
        Node childLeft = internalOf(arr, childLeftIdx);

        int childRightIdx = 2 * idx + 2;
        Node childRight = internalOf(arr, childRightIdx);

        return Node.of(key, childLeft, childRight);
    }

}
