package old.tikets_count;

import java.util.Objects;

public class TicketsCounter {

    public int countSlow(Integer[] queue, int customerIndex) {
        Objects.checkIndex(customerIndex, queue.length);

        int totalIterations = 0;

        while (queue[customerIndex] != 0) {
            for (int idx = 0; idx < queue.length; ++idx) {
                int currentTickets = queue[idx];
                if(currentTickets == 0) { // кто купил, ушел из очереди
                    continue;
                }

                --queue[idx];
                ++totalIterations;

                currentTickets = queue[idx];
                if(currentTickets == 0 && idx == customerIndex) { // если заданный чел купил все что хотел - завершаем
                    break;
                }
            }
        }

        return totalIterations;
    }

    public int countFast(Integer[] queue, int customerIndex) {
        Objects.checkIndex(customerIndex, queue.length);

        int totalIterations = 0;
        for(int idxBefore = 0; idxBefore < customerIndex; ++idxBefore) {
            int targetCustomerTicketsCount = queue[customerIndex];
            int beforeCustomerTicketsCount = queue[idxBefore];
            if(beforeCustomerTicketsCount <= targetCustomerTicketsCount) {
                totalIterations += beforeCustomerTicketsCount;
            } else {
                totalIterations += targetCustomerTicketsCount;
            }
        }

        totalIterations += queue[customerIndex];

        for (int idxAfter = customerIndex+1; idxAfter < queue.length; ++idxAfter) {
            int targetCustomerTicketsCount = queue[customerIndex];
            int afterCustomerTicketsCount = queue[idxAfter];

            if(afterCustomerTicketsCount < targetCustomerTicketsCount) {
                totalIterations += afterCustomerTicketsCount;
            } else {
                totalIterations += (targetCustomerTicketsCount - 1);
            }
        }

        return totalIterations;
    }

}
