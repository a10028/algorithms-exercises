package old.search_tree_check;

import java.util.Objects;

public class BinarySearchTreeCheck {

    public static boolean isValid(Node root) {
        Objects.requireNonNull(root);
        if(root.getLeftChild() == null && root.getRightChild() == null) {
            return true;
        }

        if(root.getLeftChild() != null) {
            boolean isValid = isValidLeftChild(root.getValue(), root.getLeftChild());
            if(!isValid) {
                return false;
            }
        }

        if(root.getRightChild() != null) {
            boolean isValid = isValidRightChild(root.getValue(), root.getRightChild());
            if(!isValid) {
                return false;
            }
        }

        return true;
    }

    private static boolean isValidLeftChild(int parentValue, Node left) {
        if(left == null) {
            return true;
        }

        if(left.getValue() >= parentValue) {
            return false;
        }

        boolean isValid = isValidLeftChild(left.getValue(), left.getLeftChild());
        if(!isValid) {
            return false;
        }

        isValid = isValidRightChild(left.getValue(), left.getRightChild());
        if(!isValid) {
            return false;
        }

        return true;
    }

    private static boolean isValidRightChild(int parentValue, Node right) {
        if(right == null) {
            return true;
        }
        if(right.getValue() <= parentValue) {
            return false;
        }

        boolean isValid = isValidRightChild(right.getValue(), right.getRightChild());
        if(!isValid) {
            return false;
        }

        isValid = isValidLeftChild(right.getValue(), right.getLeftChild());
        if(!isValid) {
            return false;
        }

        return true;
    }


}
