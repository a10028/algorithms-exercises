package old.search_tree_check;

public class TreeUtils {

    public static Node parse(int[] nodesArray) {
        if(nodesArray.length == 0) {
            return null;
        }
        int height = 0;

        // 2t+1
        // 2t-1

        // 0      1  1      2  2  2  2      3 3 3  3  3 3 3 3
        //90     50 70     25 60 69 81      1 1 1  1  1 1 1 1
        //0       1  2      3  4  5  6      7 8 9 10

        //Node left = buildNodeLeft(nodesArray, height + 1);
        //Node right = buildNodeRight(nodesArray, height + 1);

        Node root = Node.builder()
                .value(nodesArray[height])
          ///      .leftChild(left)
//                .rightChild(right)
                .build();

        return root;
    }

    private static Node buildNodeLeft(int[] nodesArray, int height, int delta) {
        int levelSize = (int) Math.pow(2, height);
        int currentLeftNodeIdx = levelSize - 1;

        Node left = buildNodeLeft(nodesArray, height + 1, 0);
        Node right = buildNodeRight(nodesArray, height + 1, 0);

        Node newNode = Node.builder()
                .value(nodesArray[currentLeftNodeIdx])
                .leftChild(left)
                .rightChild(right)
                .build();

        return newNode;
    }

    private static Node buildNodeRight(int[] nodesArray, int height, int delta) {
        int levelSize = (int) Math.pow(2, height);
        int currentRightNodeIdx = levelSize -1 + 1;

        Node left = buildNodeLeft(nodesArray, height + 1, 0);
        Node right = buildNodeRight(nodesArray, height + 1, 0);

        Node newNode = Node.builder()
                .value(nodesArray[currentRightNodeIdx])
                .leftChild(left)
                .rightChild(right)
                .build();

        return newNode;
    }

    private static void  printlnArr(int[] nodesArray, int start, int end) {
    }

}
