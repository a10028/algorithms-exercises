package old.search_tree_check;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Node {
    private Integer value;
    private Node parent;
    private Node leftChild;
    private Node rightChild;
}
