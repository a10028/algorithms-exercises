package old.mergesort;

import lombok.Getter;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

@Getter
public class MergeSort {

    private static final float DEFAULT_DIVISOR = .5f;
    private static final int DEFAULT_MINIMUM_WORK = 3;

    private final ForkJoinPool pool = new ForkJoinPool();

    public Object[] sort(Object[] unsorted) {

        if (isSmallTask(unsorted.length)) {
            Object[] sorted = calculate(unsorted);
            return sorted;
        }

        int sizeOfLeft = (int) (unsorted.length * DEFAULT_DIVISOR);
        Object[] leftUnsorted = new Object[sizeOfLeft];
        System.arraycopy(unsorted, 0, leftUnsorted, 0, sizeOfLeft);
        int sizeOfRight = unsorted.length - sizeOfLeft;
        Object[] rightUnsorted = new Object[sizeOfRight];
        System.arraycopy(unsorted, sizeOfLeft, rightUnsorted, 0, sizeOfRight);

        //pool.submit()
        Object[] leftSorted = sort(leftUnsorted);
        Object[] rightSorted = sort(rightUnsorted);

        // merge result
        Object[] mergedAndSorted = merge(leftSorted, rightSorted);
        return mergedAndSorted;
    }

    private Object[] merge(Object[] leftSorted, Object[] rightSorted) {

        int resultIndex = -1;
        Object[] result = new Object[leftSorted.length + rightSorted.length];
        int smallIdx = 0;
        Object[] smallArray = leftSorted.length > rightSorted.length ? rightSorted : leftSorted;
        int bigIdx = 0;
        Object[] bigArray = leftSorted.length > rightSorted.length ? leftSorted : rightSorted;

        while (bigIdx < bigArray.length) {
            Comparable bigArrayElement = (Comparable) bigArray[bigIdx];

            while (smallIdx < smallArray.length) {
                Comparable smallArrayElement = (Comparable) smallArray[smallIdx];
                int compareResult = bigArrayElement.compareTo(smallArrayElement);
                if (compareResult < 0) {
                    result[++resultIndex] = bigArrayElement;
                    ++bigIdx;
                    break;
                } else {
                    result[++resultIndex] = smallArrayElement;
                    ++smallIdx;
                }
            }

            if (smallIdx >= smallArray.length) {
                copyTail(bigArray, bigIdx, result, ++resultIndex);
                break;
            } else if (bigIdx >= bigArray.length) {
                copyTail(smallArray, smallIdx, result, ++resultIndex);
                break;
            }
        }

        return result;
    }

    private void copyTail(Object[] from, int fromStart, Object[] to, int toStart) {
        int elementsMovedFromSmallArray = fromStart;
        int elementsInSmallLeft = from.length - elementsMovedFromSmallArray;
        if (elementsInSmallLeft > 0) {
            System.arraycopy(from, fromStart, to, toStart, elementsInSmallLeft);
        }
    }

    private boolean isSmallTask(int size) {
        return size <= DEFAULT_MINIMUM_WORK;
    }

    private Object[] calculate(Object[] unsortedPart) {
        Arrays.sort(unsortedPart);
        return unsortedPart;
    }
}
