package old.braces_parsing;

public class BracesChecker {

    public static boolean check(char[] charSequence) {
        //if(StringUtils.isBlank(charSequence))

        BracesStack bracesStack = new BracesStack(new char[charSequence.length]);

        for (char current: charSequence) {
            boolean success = bracesStack.push(current);
            if(success == false) {
                return false;
            }
        }

        return true;
    }

}
