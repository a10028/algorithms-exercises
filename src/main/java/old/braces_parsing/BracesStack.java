package old.braces_parsing;

import lombok.Getter;

@Getter
public class BracesStack {
    private final char BR_LEFT = '(';
    private final char BR_RIGHT = ')';

    private int current = -1;
    private char[] values;
    private char last = Character.MIN_VALUE;

    public BracesStack(char[] values) {
        this.values = values;
    }

    public boolean push(char value) {
        if (value == Character.MIN_VALUE) {
            return true;
        } else if (value != BR_LEFT && value != BR_RIGHT) {
            return true;
        }

        if (current == -1) {
            add(value);
            return true;
        }

        if (last == BR_LEFT && value == BR_LEFT) {
            add(value);
            return true;
        }

        if (last == BR_LEFT && value == BR_RIGHT) {
            values[current] = Character.MIN_VALUE;
            --current;
            if(current >= 0) {
                last = values[current];
            }
            return true;
        }

        return false;
    }

    public boolean isValid() {
        return current < 0;
    }

    private void add(char value) {
        ++current;
        last = value;
        values[current] = last;
    }
}
