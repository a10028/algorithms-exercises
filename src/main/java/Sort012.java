
/*
Есть массив int, в котором могут быть только цифры от 0 до 2.
Реализовать сортировку этого массива за минимально возможное время.
Надо решить за  O(N), где N - длина массива.
Надо уметь по запросу сделать для более общего случая, где цифры могут быть от 0 до K.
*/

// time - o(n)
// space - o(1)
public class Sort012 {

    public static final int MIN_VAL = 0;
    public static final int MAX_VAL = 2;

    private int leftIdx;
    private int rightIdx;
    private int leftCommited;
    private int rightCommited;
    private long[] arr;

    public static void sort(long[] arr) {
        new Sort012(arr).sort();
    }

    public Sort012(long[] arr) {
        this.leftIdx = 0;
        this.rightIdx = arr.length - 1;
        this.leftCommited = -1;
        this.rightCommited = arr.length;
        this.arr = arr;
    }

    public long[] sort() {
        if(arr == null || arr.length == 0 || arr.length == 1) {
            return arr;
        }

        while (leftIdx <= rightIdx) {
            long leftElem = arr[leftIdx];
            long rightElem = arr[rightIdx];

            if (leftElem == MIN_VAL && rightElem == MAX_VAL) {
                handleLeftMin();
                handleRightMax();
                continue;
            }

            if (rightElem == MIN_VAL && leftElem == MAX_VAL) {
                swap();
                handleLeftMin();
                handleRightMax();
                continue;
            }

            if (leftElem == MIN_VAL) {
                handleLeftMin();
                continue;
            }

            if (rightElem == MAX_VAL) {
                handleRightMax();
                continue;
            }

            if (leftElem == MAX_VAL) {
                swap();
                handleRightMax();
                continue;
            }

            if (rightElem == MIN_VAL) {
                swap();
                handleLeftMin();
                continue;
            }

            if (leftElem == rightElem) {
                ++leftIdx;
                --rightIdx;
                continue;
            } else {
                throw new IllegalStateException();
            }
        }

        return arr;
    }

    private void handleLeftMin() {
        long leftElem = arr[leftIdx];
        if (leftIdx == leftCommited) {
            ++leftIdx;
            ++leftCommited;
        } else {
            ++leftCommited;
            long temp = arr[leftCommited];
            arr[leftCommited] = leftElem;
            arr[leftIdx] = temp;
            if (leftCommited == leftIdx) {
                ++leftIdx;
            }
        }
    }

    private void handleRightMax() {
        long rightElem = arr[rightIdx];
        if (rightIdx == rightCommited) {
            --rightIdx;
            --rightCommited;
        } else {
            --rightCommited;
            long temp = arr[rightCommited];
            arr[rightCommited] = rightElem;
            arr[rightIdx] = temp;
            if (rightCommited == rightIdx) {
                --rightIdx;
            }
        }
    }

    private void swap() {
        long temp = arr[rightIdx];
        arr[rightIdx] = arr[leftIdx];
        arr[leftIdx] = temp;
    }
}
