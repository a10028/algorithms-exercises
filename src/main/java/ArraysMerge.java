import java.util.Arrays;
import java.util.PriorityQueue;

/*

Реализовать ту же программу для map/reduce: строка разбивается произвольным образом на подстроки,
необходимо для каждой подстроки рассчитать определенные параметры,
и затем объединить их и ответить на вопрос исходной задачи. Надо решить за N.
(другими словами: на вход дается массив подстрок (вместе они образуют входную строку задачи 1),
используя паттер map/reduce вам неообходимо ответить на вопрос задачи (вернуть true/false)

*/

// time - o(n*log(n))
// space - o(n), т.к. выделяем доп место под слияние, в данном случае это очередь
public class ArraysMerge {

    public static long[] merge(long[]... arrs) {
        final PriorityQueue<Long> pq = new PriorityQueue<>(Long::compareTo);

        Arrays.stream(arrs).forEach(arr ->
            Arrays.stream(arr).forEach(pq::add)
        );

        int pqSize = pq.size();
        long[] result = new long[pqSize];
        int idx = 0;
        while (idx < pqSize) {
            result[idx] = pq.poll();
            ++idx;
        }

        return result;
    }

}
