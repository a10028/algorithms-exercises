import java.util.Arrays;
import java.util.Objects;

/*

Дан отсортированный массив состоящий из парных элементов, но у одного элемента нет пары.
Например: [1,1, 3,3, 4, 6, 6] - нужно найти элемент без пары за минимально возможное время.
Нужно решить за O(Log(N))
*/

// time - o(log(n))
// space - o(1) если не копировать кусок массива copyOfRange, иначе o(n)
public class FindNoPairedValue {

    public static Long find(long arr[]) {
        Objects.requireNonNull(arr);
        if(arr.length == 1) {
            System.out.println("Found value alone = " + arr[0]);
            return arr[0];
        }

        int midIdx = (arr.length / 2 + 1) - 1;

        long midVal = arr[midIdx];
        if (arr[midIdx - 1] != midVal && arr[midIdx + 1] != midVal) {
            return midVal; // has no pair
        }

        // separate half and go to odd
        int leftEndIdx = -1; // excluding
        int rightStartIdx = -1;
        if(arr[midIdx - 1] == midVal) {
            leftEndIdx = midIdx - 1;
            rightStartIdx = midIdx + 1;
        } else if(arr[midIdx + 1] == midVal) {
            leftEndIdx = midIdx;
            rightStartIdx = midIdx + 2;
        } else {
            throw new IllegalStateException();
        }

        int leftSize = leftEndIdx;
        if((leftSize % 2) != 0) {
            long[] leftPart = Arrays.copyOfRange(arr, 0, leftEndIdx);
            Long targetLeft = find(leftPart);
            if(targetLeft != null) {
                return targetLeft;
            }
        }

        int rigthSize = arr.length - rightStartIdx;
        if((rigthSize % 2) != 0) {
            long[] rightPart = Arrays.copyOfRange(arr, rightStartIdx, arr.length);
            Long targetRight = find(rightPart);
            if(targetRight != null) {
                return targetRight;
            }
        }

        throw new IllegalStateException("This array has no single value");
    }

}
