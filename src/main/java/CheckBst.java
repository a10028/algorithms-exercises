import util.Node;

import java.util.Objects;

/*
Проверка дерева на BST
time - o(n)
space - o(1)
*/
public class CheckBst {

    public static boolean check(Node tree) {
        Objects.requireNonNull(tree);
        return visit(tree, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    private static boolean visit(Node node, long min, long max) {
        if (node == null) {
            return true;
        }

        long key = node.getKey();
        if (min <= key && key <= max) {
            boolean isValidLeft = visit(node.getLeft(), min, key - 1);
            if (!isValidLeft) {
                return false;
            }

            return visit(node.getRight(), key + 1, max);
        }

        return false;
    }
}
