import util.Node;

/*
Есть бинарное дерево поиска с N числами и заданное число.
Нужно найти в дереве первое число, которое строго больше этого заданного.
При этом само заданное число может в дереве не присутствовать.
Должно работать за O(Log N).
*/

// time - o(log(n))
// space - o(1)
public class BstFirstHigherKeyFinder {

    private Node firstHigherNode = null;

    public static Node findFirstKeyHigher(Node bst, long target) {
        BstFirstHigherKeyFinder finder = new BstFirstHigherKeyFinder();
        finder.findFirstKeyHigherInternal(bst, target);
        return finder.firstHigherNode;
    }

    public void findFirstKeyHigherInternal(Node currentNode, long target) {
        if(currentNode == null) {
            return;
        }

        long currentKey = currentNode.getKey();
        if(currentNode.getLeft() == null && currentNode.getRight() == null) {
            if(currentKey > target) {
                storeMin(currentNode);
                return;
            } else {
                return;
            }
        }

        if(target == currentKey) {
            Node rightChild = currentNode.getRight();
            findFirstKeyHigherInternal(rightChild, target);
        } else if(target > currentKey) {
            Node rightChild = currentNode.getRight();
            findFirstKeyHigherInternal(rightChild, target);
        } else if(target < currentKey) {
            storeMin(currentNode);
            Node leftChild = currentNode.getLeft();
            findFirstKeyHigherInternal(leftChild, target);
        }
    }

    private void storeMin(Node node) {
        if(node == null) {
            return;
        }
        if(firstHigherNode == null) {
            firstHigherNode = node;
        }

        if(firstHigherNode.getKey() > node.getKey()) {
            firstHigherNode = node;
        }
    }

}
