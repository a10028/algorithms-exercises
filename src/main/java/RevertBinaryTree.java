import util.Node;

//Отразить бинарное дерево

// time - o(n)
// space - o(1)
public class RevertBinaryTree {

    public static Node revert(Node currentNode) {
        if(currentNode == null) {
            return null;
        }

        Node rightChild = currentNode.getRight();
        Node visitedRight = revert(rightChild);

        Node leftChild = currentNode.getLeft();
        Node visitedLeft = revert(leftChild);

        Node revertedLeftChild = visitedRight;
        Node revertedRightChild = visitedLeft;
        return Node.of(currentNode.getKey(), revertedLeftChild, revertedRightChild);
    }
}
