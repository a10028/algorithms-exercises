import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/*
Написать простую реализацию HashMap (только базовые методы, без, например, перестраивания таблицы)
*/

// o(1) - лучший случай, когда добавляем в начало списка
// o(n) - худший случай, когда хешкод плохой и все хранится в обном бакете
// o(n/k) - случай когда, распределено равномерно по бакетам, к- количество бакетов

// space - o(1)
public class MyHashMap<K, V> {

    private static int BUCKETS_CNT = 10;
    private int size = 0;

    @SuppressWarnings("unchecked")
    private List<Entry<K, V>>[] buckets = (List<Entry<K, V>>[]) Array.newInstance(List.class, BUCKETS_CNT);

    public void add(K key, V val) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(val);

        List<Entry<K, V>> bucket = findBucket(key);

        Entry<K, V> item = Entry.of(key, val);
        boolean removed = bucket.remove(item);
        bucket.add(item);
        if (!removed) {
            ++size;
        }
    }

    public V find(K key) {
        Objects.requireNonNull(key);

        int idx = key.hashCode() % BUCKETS_CNT;
        List<Entry<K, V>> bucket = buckets[idx];
        if (bucket == null) {
            return null;
        }

        for (Entry<K, V> entry : bucket) {
            if (entry.key.equals(key)) {
                return entry.val;
            }
        }

        return null;
    }

    public V remove(K key) {
        Objects.requireNonNull(key);

        int idx = key.hashCode() % BUCKETS_CNT;
        List<Entry<K, V>> bucket = buckets[idx];
        if (bucket == null) {
            return null;
        }

        Iterator<Entry<K, V>> iter = bucket.iterator();
        while (iter.hasNext()){
            Entry<K, V> current = iter.next();
            if(current.key.equals(key)) {
                iter.remove();
                --size;
                return current.val;
            }
        }

        return null;
    }

    public int size() {
        return size;
    }

    private List<Entry<K, V>> findBucket(K key) {
        int idx = key.hashCode() % BUCKETS_CNT;
        List<Entry<K, V>> bucket = buckets[idx];
        if (bucket == null) {
            bucket = buckets[idx] = new ArrayList<Entry<K, V>>();
        }

        return bucket;
    }

    public static class Entry<K, V> {
        private K key;
        private V val;

        public static <K, V> Entry<K, V> of(K key, V val) {
            Entry<K, V> entry = new Entry<>();
            entry.key = key;
            entry.val = val;
            return entry;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Entry)) return false;
            Entry<?, ?> entry = (Entry<?, ?>) o;
            return key.equals(entry.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }
    }

}
