import util.Node;

import java.util.Arrays;
import java.util.Objects;

/*
Есть отсортированный массив. Нужно по нему построить сбалансированное бинарное дерево поиска.
Должно работать за O(N).
*/

// time - o(n)
// space - o(1), если не копировать куски массива copyOfRange, так получается o(n)
public class ArrayToBinarySearchTree {

    public static Node createTree(long[] sortedArr) {
        Objects.requireNonNull(sortedArr);
        if (sortedArr.length == 0) {
            return null;
        }
        if (sortedArr.length == 1) {
            return Node.of(sortedArr[0], null, null);
        }

        int midIdx = sortedArr.length / 2;
        long midVal = sortedArr[midIdx]; // tree-key

        // lower vals
        long[] leftArr = Arrays.copyOfRange(sortedArr, 0, midIdx);
        Node leftChild = createTree(leftArr);

        // higher vals
        Node rightChild = null;
        int rightStart = midIdx + 1;
        if(rightStart < sortedArr.length) {
            long[] rightArr = Arrays.copyOfRange(sortedArr, midIdx + 1, sortedArr.length);
            rightChild = createTree(rightArr);
        }

        return Node.of(midVal, leftChild, rightChild);
    }
}
