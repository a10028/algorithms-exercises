import util.Node;

import java.util.LinkedList;
import java.util.List;
/*
Есть бинарное дерево поиска.
Реализовать поиск интервала от минимального до максимального, вернуть в виде отсортированного списка.
*/

// time - o(n)
// space - o(1)
public class BstRangeFinderInfix {

    private List<Long> range = new LinkedList<>();
    private long min;
    private long max;

    private BstRangeFinderInfix(long min, long max) {
        this.min = min;
        this.max = max;
    }

    public static List<Long> findRange(Node bst, long min, long max) {
        BstRangeFinderInfix finder = new BstRangeFinderInfix(min, max);
        finder.infixVisit(bst);
        return finder.range;
    }

    private void infixVisit(Node node) {
        if (node == null) {
            return;
        }

        long currentKey = node.getKey();
        if (min < currentKey) {
            infixVisit(node.getLeft());
        }

        if (min <= currentKey && currentKey <= max) {
            range.add(currentKey);
        }

        if (currentKey < max) {
            infixVisit(node.getRight());
        }
    }

}
