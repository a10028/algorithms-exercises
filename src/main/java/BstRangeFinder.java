import util.Node;

import java.util.LinkedList;
import java.util.List;

public class BstRangeFinder {

    private List<Long> range = new LinkedList<>();
    private long min;
    private long max;

    private BstRangeFinder(long min, long max) {
        this.min = min;
        this.max = max;
    }

    public static List<Long> findRange(Node bst, long min, long max) {
        BstRangeFinder finder = new BstRangeFinder(min, max);
        finder.visit(bst);
        return finder.range;
    }

    private void visit(Node node) {
        if (node == null) {
            return;
        }

        long currentKey = node.getKey();

        if (currentKey <= min) {
            visit(node.getRight());
        } else if (currentKey >= max) {
            visit(node.getLeft());
        } else {
            visit(node.getLeft());
            visit(node.getRight());
        }

        if (min <= currentKey && currentKey <= max) {
            range.add(currentKey);
        }
    }
}
