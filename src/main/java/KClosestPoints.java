import java.util.Arrays;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;

public class KClosestPoints {

    public static int[][] getKClosestPoints(int[][] points, int k) {
        Map<Double, Integer[]> result = new TreeMap<>();

        Arrays.sort(points, (p1, p2) -> {
            Double sqr1 = square(p1[0], p1[1]);
            Double sqr2 = square(p2[0], p2[1]);
            return sqr1.compareTo(sqr2);
        });

        int[][] sortedPoints = points;
        return Arrays.copyOfRange(sortedPoints, 0, k);
    }

    public static int[][] getKClosestPointsPq(int[][] points, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((p1, p2) -> {
            Double sqr1 = square(p1[0], p1[1]);
            Double sqr2 = square(p2[0], p2[1]);
            return sqr2.compareTo(sqr1);
        });

        for (int idx = 0; idx < points.length; ++idx) {
            int[] curentPoint = points[idx];
            pq.add(curentPoint);
            if (pq.size() > k) {
                pq.poll();
            }
        }
        return pq.toArray(new int[0][0]);
    }


    // square((x1 - x2)^2 + (y1 - y2)^2)
    private static double square(int x1, int y1) {
        int x2 = 0;
        int y2 = 0;

        return Math.pow((x1 - x2), 2.0) + Math.pow((y1 - y2), 2.0);
    }

}